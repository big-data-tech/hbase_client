package com.gmo.hbase;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseMultiPut extends Thread{
	private Thread t;
	private String threadName;
	private int rows;

	HbaseMultiPut(String name,int num) {
		threadName = name;
		System.out.println("Creating bb " + threadName);
		rows = num;
	}

	public void run() {
		System.out.println("Running " + threadName);
		Random randomGenerator = new Random();
		HConnection connection = null;
		try {
			long start = System.currentTimeMillis();
			connection = Config.getCnn();
			HTableInterface table = connection.getTable("hb_user_cookie");
			table.setAutoFlush(false);
			table.setWriteBufferSize(1024*1024*20);
			
			for(int i = 0; i < rows ; i ++ ){
				Put put = new Put(Bytes.toBytes(i + "cookie0001"));
				put.add(Bytes.toBytes("fal"), Bytes.toBytes("qual"), Bytes.toBytes(""+i));
				table.put(put);
			}
			
			long end = System.currentTimeMillis();
			System.out.println("Time: " + (end - start)/100);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(connection!=null){
				try {
					connection.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this,threadName);
			t.start();
		}
	}
}
