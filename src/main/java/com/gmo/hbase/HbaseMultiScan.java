package com.gmo.hbase;

import java.io.IOException;

import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseMultiScan  extends Thread{
	private Thread t;
	private String threadName;
	private int rows;

	HbaseMultiScan(String name,int num) {
		threadName = name;
		System.out.println("Creating bb " + threadName);
		rows = num;
	}

	public void run() {
		System.out.println("Running " + threadName);
		try {
			long start = System.currentTimeMillis();
			HTableInterface table = Config.getConfig();
			
			
			for(int i = 0; i < rows ; i ++ ){
				
				Scan s = new Scan(Bytes.toBytes(i),Bytes.toBytes(i+1));
//			    s.addColumn(Bytes.toBytes("fal"), Bytes.toBytes("qual"));
				s.setCaching(500);
//				s.setCacheBlocks(true);
				System.out.println("Caching " + s.getCaching());
			    ResultScanner scanner = table.getScanner(s);
			    
			    try {
			      // Scanners return Result instances.
			      // Now, for the actual iteration. One way is to use a while loop like so:
//			      for (Result rr = scanner.next(); rr != null; rr = scanner.next()) {
//			        // print out the row we found and the columns we were looking for
//			        System.out.println("Found row: " + rr);
//			      }

			      // The other approach is to use a foreach loop. Scanners are iterable!
			      // for (Result rr : scanner) {
			      //   System.out.println("Found row: " + rr);
			      // }
			    } finally {
			      // Make sure you close your scanners when you are done!
			      // Thats why we have it inside a try/finally clause
			      scanner.close();
			    }
			}
			
			
			long end = System.currentTimeMillis();
			System.out.println("Time: " + (end - start)/100);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this,threadName);
			t.start();
		}
	}
}
