package com.gmo.hbase;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) throws IOException {
		System.out.println("Hello World!");
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.zookeeper.quorum",
				"10.112.21.72,10.112.21.73,10.112.21.74");
		HConnection connection = HConnectionManager.createConnection(conf);
		HTableInterface table = connection.getTable("hb_user_cookie");

		Get get = new Get(Bytes.toBytes("cookie0001"));

		
		
		
		Result r = table.get(get);

		byte[] optout = r.getValue(Bytes.toBytes("optout"),
				Bytes.toBytes("optout"));
		String valueStr = Bytes.toString(optout);
		System.out.println("GET: " + valueStr);

		String valueTime = Bytes.toString(r.getValue(Bytes.toBytes("optout"),
				Bytes.toBytes("output_time")));
		System.out.println("GET: " + valueTime);
		
		
		table.close();
		connection.close();
	}
}
