package com.gmo.hbase;



/**
 * @author long-ta
 * Date : 2014/06/23
 * mvn compile exec:java -Dexec.mainClass="com.gmo.hbase.HbaseTestTools" -Dexec.args="put 10000" >> logs.log
 * mvn compile exec:java -Dexec.mainClass="com.gmo.hbase.HbaseTestTools" -Dexec.args="get 10000" >> logs.log
 * mvn compile exec:java -Dexec.mainClass="com.gmo.hbase.HbaseTestTools" -Dexec.args="scan 10000" >> logs.log
 */
public class HbaseTestTools {
	
	private static int THREAD_NUMS = 100;
	
	public static void main(String[] args){
		if( args.length == 0){
			System.out.println("Usage: method");
			System.out.println("put : Multi put");
			System.out.println("get : Multi get");
			System.out.println("scan : Multi scan");
			System.exit(0);
		}
		String method = args[0];
		int rows = Config.NUM;
		if(args.length == 2){
			rows = Integer.parseInt(args[1]);
		}
//		String method = "put";
		if(method.equals("put")){
			for(int i = 0 ; i < THREAD_NUMS ; i ++){
				HbaseMultiPut put = new HbaseMultiPut("Put",rows);
				put.start();
			}
			
		}else if(method.equals("get")){
			for(int i = 0 ; i < THREAD_NUMS ; i ++){
				HbaseMultiGet get = new HbaseMultiGet("Get", rows);
				get.start();
			}			
		}else if (method.equals("scan")){
			for(int i = 0 ; i < THREAD_NUMS ; i ++){
				HbaseMultiScan scan = new HbaseMultiScan("Scan", rows);
				scan.start();
			}			
		}else {
			System.out.println("Usage: method");
			System.out.println("put : Multi put");
			System.out.println("get : Multi get");
			System.out.println("scan : Multi scan");
			System.exit(0);
		}
		
	}
}
