package com.gmo.hbase;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;

public class Config {

	public static int NUM = 1000;
	public static int CNN = 50;
	
	public static HTableInterface getConfig() throws IOException{
		Configuration conf = HBaseConfiguration.create();
		
		conf.set("hbase.zookeeper.quorum",
				"10.112.21.72,10.112.21.73,10.112.21.74");
		HConnection connection = HConnectionManager.createConnection(conf);
		HTableInterface table = connection.getTable("hb_user_cookie");
		table.setAutoFlush(false);
		table.setWriteBufferSize(1024*1024*CNN);
		
		return table;
	}
	
	public static HConnection getCnn() throws IOException{
		Configuration conf = HBaseConfiguration.create();
		
		conf.set("hbase.zookeeper.quorum",
				"10.112.21.72,10.112.21.73,10.112.21.74");
		HConnection connection = HConnectionManager.createConnection(conf);
		
		return connection;
	}
}
