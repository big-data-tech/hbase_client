package com.gmo.hbase;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseMultiGet extends Thread{
	private Thread t;
	private String threadName;
	private int rows;

	HbaseMultiGet(String name,int num) {
		threadName = name;
		System.out.println("Creating bb " + threadName);
		rows = num;
	}

	public void run() {
		System.out.println("Running " + threadName);
		Random randomGenerator = new Random();
		try {
			long start = System.currentTimeMillis();
			HTableInterface table = Config.getConfig();
			
			for(int i = 0; i < rows ; i ++ ){
				Get get = new Get(Bytes.toBytes(randomGenerator.nextInt(1000) + "cookie0001"));
//				put.add(Bytes.toBytes("fal"), Bytes.toBytes("qual"), Bytes.toBytes(""+i));
				table.get(get);
			}
			
			long end = System.currentTimeMillis();
			System.out.println("Time: " + (end - start)/100);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this,threadName);
			t.start();
		}
	}
}
